define({ "api": [
  {
    "type": "get",
    "url": "/api/1.0/products/:pk/price",
    "title": "Get price",
    "permission": [
      {
        "name": "Super admin"
      }
    ],
    "description": "<p>Return product price by date</p>",
    "group": "Products",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Access Authentication token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":pk",
            "description": "<p>Product ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "date",
            "description": "<p>Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/1.0/products/:pk/price"
      }
    ],
    "version": "0.0.0",
    "filename": "api/products/views.py",
    "groupTitle": "Products",
    "name": "GetApi10ProductsPkPrice"
  },
  {
    "type": "get",
    "url": "/api/1.0/products/:pk/price/avg/",
    "title": "Get average price",
    "permission": [
      {
        "name": "Super admin"
      }
    ],
    "description": "<p>Return average product price by date range</p>",
    "group": "Products",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Access Authentication token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":pk",
            "description": "<p>Product ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "date_start",
            "description": "<p>Start date</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "date_end",
            "description": "<p>End date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/1.0/products/:pk/price/avg/"
      }
    ],
    "version": "0.0.0",
    "filename": "api/products/views.py",
    "groupTitle": "Products",
    "name": "GetApi10ProductsPkPriceAvg"
  },
  {
    "type": "post",
    "url": "/api/1.0/products/:pk/price/",
    "title": "Set price",
    "permission": [
      {
        "name": "Super admin"
      }
    ],
    "description": "<p>Set product price by current date</p>",
    "group": "Products",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Access Authentication token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":pk",
            "description": "<p>Product ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "price",
            "description": "<p>Price of product</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/1.0/products/:pk/price/"
      }
    ],
    "version": "0.0.0",
    "filename": "api/products/views.py",
    "groupTitle": "Products",
    "name": "PostApi10ProductsPkPrice"
  },
  {
    "type": "put",
    "url": "/api/1.0/products/:pk/price/",
    "title": "Update price",
    "permission": [
      {
        "name": "Super admin"
      }
    ],
    "description": "<p>Update product price by date range</p>",
    "group": "Products",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Access Authentication token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": ":pk",
            "description": "<p>Product ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "price",
            "description": "<p>Price of product</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "date_start",
            "description": "<p>Start date</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "date_end",
            "description": "<p>End date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "/api/1.0/products/:pk/price/"
      }
    ],
    "version": "0.0.0",
    "filename": "api/products/views.py",
    "groupTitle": "Products",
    "name": "PutApi10ProductsPkPrice"
  }
] });
