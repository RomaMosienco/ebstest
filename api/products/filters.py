from django_filters import rest_framework as filters
from api.products.models import ProductPrice


class PriceAverageFilter(filters.FilterSet):
    date_start = filters.DateFilter(field_name='created_at', lookup_expr='gte')
    date_end = filters.DateFilter(field_name='created_at', lookup_expr='lte')

    class Meta:
        model = ProductPrice
        fields = ['date_start', 'date_end']
