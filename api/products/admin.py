from django.contrib import admin
from api.products.models import Product


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'created_at', 'updated_at')


admin.site.register(Product, ProductAdmin)

