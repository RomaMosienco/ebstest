from datetime import date

from rest_framework import serializers
from api.products.models import ProductPrice
from api.common.errors import ValidationErrorMessage


class ProductPriceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductPrice
        fields = '__all__'


class ProductPriceCreate(serializers.Serializer):
    price = serializers.FloatField()
    date = serializers.DateField(required=False)

    def validate_price(self, value):
        if int(value) < 0:
            raise serializers.ValidationError(ValidationErrorMessage.NEGATIVE_NUMBER)
        else:
            return value

    def validate_date(self, value):
        if value > date.today():
            raise serializers.ValidationError(ValidationErrorMessage.WRONG_DATE)
        else:
            return value


class ProductPriceUpdate(ProductPriceCreate):
    date_start = serializers.DateField()
    date_end = serializers.DateField()


class ProductPerDate(serializers.Serializer):
    date = serializers.DateField()

