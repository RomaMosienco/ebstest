from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APIRequestFactory

from api.products.models import ProductPrice, Product
from api.products.views import ProductPriceView


class TestProductPrice(TestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_superuser(
            username='john_doe', email='john_doe@gmail.com', password='qwerty')
        self.client = APIClient()
        self.token = Token.objects.create(user=self.user)
        self.factory = APIRequestFactory()
        self.product = Product.objects.create(
            name='Test product 1',
            code=1243,
            description='Lorem ipsum'
        )

    def test_set_product_price(self):
        request = self.factory.post(reverse('product-price', kwargs={"pk": 1}), {
            "price": 15.20
        }, HTTP_AUTHORIZATION='Token ' + self.token.key)
        request.user = self.user
        response = ProductPriceView.as_view()(request, 1)

        self.assertEquals(response.status_code, 201)
        self.assertEquals(response.data.get('id'), 1)
        self.assertEquals(response.data.get('price'), 15.20)

    def test_get_product_price_by_date(self):
        ProductPrice.objects.create(
            product=self.product,
            price=10.50,
            created_at='2020-01-01',
        )
        response = self.client.get(reverse('product-price', kwargs={"pk": 1}), {
            "date": "2020-01-01"
        }, HTTP_AUTHORIZATION='Token ' + self.token.key)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data.get('id'), 1)
        self.assertEquals(response.data.get('created_at'), "2020-01-01")
        self.assertEquals(response.data.get('price'), 10.50)

    def test_product_average_price(self):
        price1 = ProductPrice.objects.create(
            product=self.product,
            price=10,
            created_at="2020-01-01"
        )
        price2 = ProductPrice.objects.create(
            product=self.product,
            price=15,
            created_at="2020-01-02"
        )
        price3 = ProductPrice.objects.create(
            product=self.product,
            price=20,
            created_at="2020-01-03"
        )

        response = self.client.get(reverse('product-avg-price', kwargs={"pk": 1}),
                                   HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.data.get('price__avg'), 15)
