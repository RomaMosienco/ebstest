from datetime import date
from django.db.models import Avg

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAdminUser

from django_filters.rest_framework import DjangoFilterBackend

from api.products.models import Product, ProductPrice, ProductPriceActivity
from api.products.serializers import (
    ProductPerDate, ProductPriceSerializer, ProductPriceUpdate, ProductPriceCreate
)
from api.products.filters import PriceAverageFilter


class ProductPriceView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request, pk: int) -> Response:
        """
        @api {get} /api/1.0/products/:pk/price Get price
        @apiPermission Super admin
        @apiDescription Return product price by date
        @apiGroup Products
        @apiHeader Authorization Basic Access Authentication token.
        @apiParam {Number} :pk Product ID
        @apiParam {Date} date Date
        @apiSampleRequest /api/1.0/products/:pk/price
        """
        serializer = ProductPerDate(data=request.query_params)
        if serializer.is_valid():
            product = get_object_or_404(Product, pk=pk)
            queryset = ProductPrice.objects.filter(product=product, created_at__lte=serializer.data.get('date')).first()
            return Response(ProductPriceSerializer(queryset).data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk: int) -> Response:
        """
        @api {post} /api/1.0/products/:pk/price/ Set price
        @apiPermission Super admin
        @apiDescription Set product price by current date
        @apiGroup Products
        @apiHeader Authorization Basic Access Authentication token.
        @apiParam {Number} :pk Product ID
        @apiParam {Float} price Price of product
        @apiSampleRequest /api/1.0/products/:pk/price/
        """
        serializer = ProductPriceCreate(data=request.data)
        if serializer.is_valid():
            product = get_object_or_404(Product, pk=pk)
            price_date = date.today()
            if serializer.data.get('date'):
                price_date = serializer.data.get('date')
            price = ProductPrice.objects.update_or_create(created_at=price_date, defaults={
                "product": product,
                "price": serializer.data.get('price')
            })
            ProductPriceActivity.save_price_activity(request, pk)
            return Response(ProductPriceSerializer(price[0]).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk: int) -> Response:
        """
        @api {put} /api/1.0/products/:pk/price/ Update price
        @apiPermission Super admin
        @apiDescription Update product price by date range
        @apiGroup Products
        @apiHeader Authorization Basic Access Authentication token.
        @apiParam {Number} :pk Product ID
        @apiParam {Float} price Price of product
        @apiParam {Date} date_start Start date
        @apiParam {Date} date_end End date
        @apiSampleRequest /api/1.0/products/:pk/price/
        """
        serializer = ProductPriceUpdate(data=request.data)
        if serializer.is_valid():
            product = get_object_or_404(Product, pk=pk)
            queryset = ProductPrice.objects.filter(product=product,
                                                   created_at__gte=serializer.data.get('date_start'),
                                                   created_at__lte=serializer.data.get('date_end'))
            queryset.update(price=serializer.data.get('price'))
            ProductPriceActivity.save_price_activity(request, pk)
            return Response(ProductPriceSerializer(queryset, many=True).data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductPriceAverageView(APIView):
    permission_classes = [IsAdminUser]
    filter_backends = [
        DjangoFilterBackend,
    ]
    filterset_class = PriceAverageFilter

    def filter_queryset(self, queryset):
        for backend in list(self.filter_backends):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset

    def get_queryset(self, request, pk):
        return ProductPrice.objects.filter(product_id=pk)

    def get(self, request, pk) -> Response:
        """
        @api {get} /api/1.0/products/:pk/price/avg/ Get average price
        @apiPermission Super admin
        @apiDescription Return average product price by date range
        @apiGroup Products
        @apiHeader Authorization Basic Access Authentication token.
        @apiParam {Number} :pk Product ID
        @apiParam {Date} [date_start] Start date
        @apiParam {Date} [date_end] End date
        @apiSampleRequest /api/1.0/products/:pk/price/avg/
        """
        queryset = self.filter_queryset(self.get_queryset(request, pk))
        avg = queryset.aggregate(Avg('price'))
        return Response(avg, status=status.HTTP_200_OK)
