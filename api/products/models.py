import json
from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):
    name = models.CharField(max_length=256)
    code = models.IntegerField()
    description = models.CharField(max_length=1000)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.name


class ProductPrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.FloatField()
    created_at = models.DateField()
    updated_at = models.DateField(auto_now=True)


class ProductPriceActivity(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    url = models.TextField()
    method = models.CharField(max_length=25)
    view = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    data = models.TextField(null=True)

    @classmethod
    def save_price_activity(cls, request, product_id: int):
        data = None
        if request.method == "POST" or request.method == "PUT":
            data = json.dumps(request.POST)
        view_name = request.resolver_match.func.__name__ if request.resolver_match else "UNDEFINED"
        return cls.objects.create(user=request.user,
                                  product_id=product_id,
                                  url=request.build_absolute_uri(),
                                  method=request.method,
                                  data=data,
                                  view=view_name)







