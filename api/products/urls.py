from django.urls import path
from api.products.views import ProductPriceView, ProductPriceAverageView

urlpatterns = [
    path('<int:pk>/price/', ProductPriceView.as_view(), name='product-price'),
    path('<int:pk>/price/avg/', ProductPriceAverageView.as_view(), name='product-avg-price'),
]
